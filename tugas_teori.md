## Apa itu HTML?
HTML adalah bahasa markup standar yang digunakan untuk membuat halaman website dan aplikasi web.

### Element, Tag, Atribut:

<p align="center">Hello World!</p>
--                            ---- 
Tag pembuka                   tag penutup
   -------------
      Atribut
                  ------------
                    Isi Tag
-----------------------------------
             Element



## Apa itu CSS?
CSS / cascading style sheets, yaitu bahasa yang digunakan untuk menentukan tampilan dan format halaman website. CSS erat kaitannya dengan styling, seperti background, warna, layout, spasi, dan animasi.

## Apa itu Javascript?
JavaScript memungkinkan menambahkan fungsionalitas yang dinamis, seperti slider, pop-up, dan galeri foto.


HTML orang tanpa baju
CSS sebagai baju
Javascript aktivitas/perilaku orang tsb

## Apa itu jQuery?
jQuery merupakan pustaka JavaScript. jQuery digunakan untuk manipulasi html dan css




Sumber: 
https://www.hostinger.co.id/tutorial/apa-itu-html
https://www.niagahoster.co.id/blog/pengertian-css/?amp
https://id.wikipedia.org/wiki/JQuery



# ==================================================

## Apa itu MVC?
MVC adalah pola arsitektuk/desain software yang terdiri atas 3 bagian yaitu View(End User), Model(langsung berhubungan dengan database), dan Controller(penghung View dan Model).

## Cara kerja MVC

    Pada bagian View melakukan permintaan informasi agar dapat bisa ditampilkan kepada pengguna.
    Permintaan tersebut kemudian diterima oleh Controller dan dikirimkan ke bagian model untuk diproses.
    Di bagian Model, informasi tersebut akan diolah dan dicari data informasinya ke dalam database yang dimiliki
    Setelahnya, Model akan memberikan kembali pada controller untuk ditampilkan hasilnya pada View.
    Controller mengambil hasil olahan yang dilakukan pada bagian model dan menatanya pada bagian View.


## Manfaat MVC
    Proses dalam pengembangan website menjadi lebih efisien
    Testing menjadi lebih mudah
    Error atau Bug Lebih Cepat dan Mudah Ditangani
    Pemeliharaan atau Maintenance Menjadi Lebih Mudah


Sumber:
https://lp2mp.uma.ac.id/apa-itu-model-view-controller-mvc/


# ======================================================

## HTTP method: Get, Post, Put, Delete

HTTP Get: method yang digunakan untuk memperoleh/mendapatkan data
	jika data ada diserver yang dituju dan bisa diakses, maka HTTP akan memberikan kode respon 200 (OK)
	jika tidak ditemukan, 404 (NOT FOUND)

HTTP Post: untuk membuat data/sumber baru.
	kode respon HTTP: jika berhasil 201 (CREATED)
	jika gagal 200 (OK) atau 204 (NO CONTENT)

HTTP Put: untuk update data/sumber yang telah ada.
	kode respon sama seperti HTTP Post

HTTP Delete: untuk operasi penghapusan sumber data pada server
	kode respon jika berhasil: 200 (OK)
	jika operasi penghapusan bergilir/antri: 202 (Accepted)
	jika tidak ada data: 404 (NOT FOUND)




    GET requests can be cached
    GET requests remain in the browser history
    GET requests can be bookmarked
    GET requests should never be used when dealing with sensitive data
    GET requests have length restrictions
    GET requests are only used to request data (not modify)



    POST requests are never cached
    POST requests do not remain in the browser history
    POST requests cannot be bookmarked
    POST requests have no restrictions on data length






The difference between POST and PUT is that PUT requests are idempotent. That is, calling the same PUT request multiple times will always produce the same result. In contrast, calling a POST request repeatedly have side effects of creating the same resource multiple times.






sumber:
https://restfulapi.net/http-methods/
https://www.w3schools.com/tags/ref_httpmethods.asp






