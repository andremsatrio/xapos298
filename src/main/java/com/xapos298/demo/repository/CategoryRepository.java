package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {
	List<Category> findByIsActive(Boolean isActive);
	
	//@Query("FROM Category WHERE lower(categoryName) LIKE %?1%  AND isActive=true")
	//@Query("SELECT c FROM Category c WHERE lower(c.categoryName) LIKE %?1%  AND isActive=true")
	//@Query("FROM Category WHERE lower(categoryName) LIKE lower(concat('%',?1,'%')) AND isActive=true")
	@Query("SELECT c FROM Category c WHERE LOWER(c.categoryName) LIKE LOWER(CONCAT('%',?1,'%')) AND isActive=true")
	List<Category> searchByKeyword(String keyword);
}
