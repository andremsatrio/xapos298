package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.xapos298.demo.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long> {
	List<Variant> findByIsActive(Boolean isActive);

	List<Variant> findByCreatedBy(String createdBy);

	List<Variant> findByIsActiveAndCreatedBy(Boolean isActive, String createdBy);

	@Query(value = "SELECT * FROM variant WHERE is_active = ?1 AND created_by = ?2", nativeQuery = true)
	List<Variant> findByActiveAndCreateBy(Boolean isActive, String createdBy);
	
	List<Variant> findByIsActiveAndCategoryId(Boolean isActive, Long categoryId);
	
	@Query("FROM Variant WHERE LOWER(variantName) LIKE LOWER(CONCAT('%',?1,'%')) AND isActive=true")
	List<Variant> searchByKeyword(String keyword);
}
