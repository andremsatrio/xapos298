package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.OrderHeaders;

public interface OrderHeaderRepository extends JpaRepository<OrderHeaders, Long> {
	@Query("SELECT MAX(id) FROM OrderHeaders")
	public Long findByMaxId();

	@Query(value = "SELECT * FROM order_header WHERE amount > 0", nativeQuery = true)
	List<OrderHeaders> findByAmountGreaterThanZero();
	
	@Query("FROM OrderHeaders WHERE LOWER(reference) LIKE LOWER(CONCAT('%',?1,'%'))")
	List<OrderHeaders> searchByKeyword(String keyword);
}
