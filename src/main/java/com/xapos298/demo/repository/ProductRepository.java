package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	List<Product> findByIsActive(Boolean isActive);
	
	@Query("FROM Product WHERE LOWER(productName) LIKE LOWER(CONCAT('%',?1,'%')) AND isActive=true")
	List<Product> searchByKeyword(String keyword);
}
