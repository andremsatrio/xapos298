package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos298.demo.model.OrderDetails;

public interface OrderDetailRepository extends JpaRepository<OrderDetails, Long> {
	List<OrderDetails> findByHeaderId(Long id);
}
