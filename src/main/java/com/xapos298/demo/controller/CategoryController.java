package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.repository.CategoryRepository;

@Controller
@RequestMapping("/category/")
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("category/index");
		List<Category> listCategory = this.categoryRepository.findAll(); // SELECT * FROM category

		// add list to the view
		view.addObject("listCategory", listCategory);
		return view;
	}

	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = new Category();
		view.addObject("category", category);
		return view;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
		if (!result.hasErrors()) {
			if (category.getId() != null) {// if edit user
				// old category
				Category oldCategory = this.categoryRepository.findById(category.getId()).orElse(null);

				oldCategory.setModifyBy("user2");
				oldCategory.setModifyDate(new Date());
				oldCategory.setCategoryInitial(category.getCategoryInitial());
				oldCategory.setCategoryName(category.getCategoryName());
				oldCategory.setIsActive(category.getIsActive());

				category = oldCategory;

			} else { // if new user
				category.setCreatedBy("user1");
				category.setCreatedDate(new Date()); // == Date now
			}

//			System.out.println(category.getId() + " " + category.getModifyBy() + " " + category.getCreatedBy());

			this.categoryRepository.save(category); // INSERT INTO ... VALUES ...
		}
		return new ModelAndView("redirect:/category/index");
	}

	@GetMapping("edit/{ids}")
	public ModelAndView edit(@PathVariable("ids") Long id) {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = this.categoryRepository.findById(id).orElse(null); // SELECT * FROM category WHERE id = {id}
		view.addObject("category", category);
		return view;
	}

	@GetMapping("delete/{ids}")
	public ModelAndView delete(@PathVariable("ids") Long id) {
		if (id != null) {
			this.categoryRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/category/index");
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("category/indexapi");
		
		return view;
	}
}
