package com.xapos298.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.OrderDetails;
import com.xapos298.demo.repository.OrderDetailRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderDetailController {
	@Autowired
	public OrderDetailRepository orderDetailRepository;

	@PostMapping("orderdetail/add")
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetails orderDetail) {
		OrderDetails orderDetailData = this.orderDetailRepository.save(orderDetail);
		if (orderDetailData.equals(orderDetail)) {
			return new ResponseEntity<>("Save item success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save failed", HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("orderdetail/header/{ids}")
	public ResponseEntity<List<OrderDetails>> getOrderDetailByHeaderId(@PathVariable("ids") Long id){
		try {
			List<OrderDetails> orderDetail = this.orderDetailRepository.findByHeaderId(id);
			return new ResponseEntity<List<OrderDetails>>(orderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<OrderDetails>>(HttpStatus.NO_CONTENT);
		}
	}
}
