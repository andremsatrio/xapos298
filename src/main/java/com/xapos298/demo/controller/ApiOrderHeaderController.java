package com.xapos298.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.OrderHeaders;
import com.xapos298.demo.repository.OrderHeaderRepository;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderHeaderController {
	@Autowired
	public OrderHeaderRepository orderHeaderRepository;

	@PostMapping("orderheader/add")
	public ResponseEntity<Object> createdReference(@RequestBody OrderHeaders orderHeader) {
		String timeDec = System.currentTimeMillis() + "";
		orderHeader.setReference(timeDec);
		orderHeader.setAmount(0L);
		OrderHeaders orderHeaderData = this.orderHeaderRepository.save(orderHeader);
		if (orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<>("Created success", HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>("Created failed", HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("maxorderheaderid")
	public ResponseEntity<Long> getMaxId() {
		try {
			Long maxId = this.orderHeaderRepository.findByMaxId();
			return new ResponseEntity<Long>(maxId, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);

		}
	}

	@PutMapping("orderheader/done")
	public ResponseEntity<Object> doneProccess(@RequestBody OrderHeaders orderHeader) {
		Long id = orderHeader.getId();
		Optional<OrderHeaders> orderHeaderData = this.orderHeaderRepository.findById(id);

		if (orderHeaderData.isPresent()) {
			orderHeader.setId(id);
			orderHeader.setReference(orderHeaderData.get().getReference());
			this.orderHeaderRepository.save(orderHeader);
			return new ResponseEntity<Object>("Order Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("orderheader")
	public ResponseEntity<List<OrderHeaders>> getAllOrderHeader() {
		try {
			List<OrderHeaders> listOrderHeader = this.orderHeaderRepository.findByAmountGreaterThanZero();
			return new ResponseEntity<List<OrderHeaders>>(listOrderHeader, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("orderheader/search/{keyword}")
	public ResponseEntity<List<OrderHeaders>> searchOrderHeader(@PathVariable("keyword") String keyword) {
		try {
			List<OrderHeaders> listCategory = this.orderHeaderRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<OrderHeaders>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
