package com.xapos298.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.Product;
import com.xapos298.demo.repository.ProductRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiProductController {
	@Autowired
	public ProductRepository productRepository;

	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct() {
		try {
			List<Product> listProduct = this.productRepository.findByIsActive(true);
			return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("product/add")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product) {
		product.setCreatedBy("user1");
		product.setCreatedDate(new Date());
		Product productData = this.productRepository.save(product);
		if(productData.equals(product)) {
			return new ResponseEntity<>("Save data successfully",HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Save failed",HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@GetMapping("product/{id}")
	public ResponseEntity<List<Product>> getProductById(@PathVariable("id") Long id){
		try {
			Optional<Product> product = this.productRepository.findById(id);
			if(product.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(product,HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		}catch (Exception e) {
			return null;
		}
	}
	
	@PutMapping("edit/product/{ids}")
	public ResponseEntity<Object> editCategory(@PathVariable("ids") Long id, @RequestBody Product product){
		Optional<Product> productData = this.productRepository.findById(id);
		if(productData.isPresent()) {
			product.setId(id);
			product.setModifyBy("user1");
			product.setModifyDate(Date.from(Instant.now()));
			product.setCreatedBy(productData.get().getCreatedBy());
			product.setCreatedDate(productData.get().getCreatedDate());
			this.productRepository.save(product);
			return new ResponseEntity<Object>("Upload Successfully",HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("delete/product/{ids}")
	public ResponseEntity<Object> deleteProduct(@PathVariable("ids") Long id){
		Optional<Product> productData = this.productRepository.findById(id);
		if(productData.isPresent()) {
			Product product = productData.get();
			product.setId(id);
			product.setIsActive(false);
			product.setModifyBy("user1");
			product.setModifyDate(Date.from(Instant.now()));
			this.productRepository.save(product);
			return new ResponseEntity<>("Delete sucessfully",HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	
	@GetMapping("product/search/{keyword}")
	public ResponseEntity<List<Product>> searchProduct(@PathVariable("keyword") String keyword) {
		try {
			List<Product> listCategory = this.productRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<Product>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
