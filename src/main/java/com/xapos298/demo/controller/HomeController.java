package com.xapos298.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {
	@GetMapping("index")
	public String index() {
		return "index.html";
	}
	@GetMapping("biodata")
	public String biodata() {
		return "biodata";
	}
	@GetMapping("form")
	public String form() {
		return "form.html";
	}
}
