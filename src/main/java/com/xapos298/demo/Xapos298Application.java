package com.xapos298.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xapos298Application {

	public static void main(String[] args) {
		SpringApplication.run(Xapos298Application.class, args);
	}

}
