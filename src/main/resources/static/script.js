function saveData() {
	// dapatkan value data
	let nama = document.getElementById('nama_lengkap').value;
	let gender = document.getElementById('gender').value;
	let alamat = document.getElementById('alamat').value;
	let agama = "";
	if (document.getElementById('p1').checked){
		agama = document.getElementById('p1').value;
	} else if(document.getElementById('p2').checked){
		agama = document.getElementById('p2').value;
	} else if(document.getElementById('p3').checked){
		agama = document.getElementById('p3').value;
	} else if(document.getElementById('p4').checked){
		agama = document.getElementById('p4').value;
	} else if(document.getElementById('p5').checked) {
		agama = document.getElementById('p5').value;
	} else {
		agama = "Belum dipilih";
	}
	
	// Tampilkan ke form HTML
	var string = "";
	string += "<p>Nama Lengkap: "+nama+"<br>";
	string += "Gender: "+gender+"<br>";
	string += "Alamat: "+alamat+"<br>";
	string += "Agama: "+agama+"</p>";
	document.getElementById('isidata').innerHTML = string;
}

function withJquery(){
	let nama = $('#nama_lengkap').val();
	let alamat = $('#alamat').val();
	let gender = $('#gender').val();
	let agama = "";
	if($('#p1')[0].checked){
		agama = $('#p1').val();
	} else if($('#p2')[0].checked){
		agama = $('#p2').val();
	} else if($('#p3')[0].checked){
		agama = $('#p3').val();
	} else if($('#p4')[0].checked){
		agama = $('#p4').val();
	} else if($('#p5')[0].checked){
		agama = $('#p5').val();
	} else {
		agama = "Belum dipilih";
	}
	
	let str = "";
	str += "<p>Nama Lengkap: "+nama+"<br>";
	str += "Gender: "+gender+"<br>";
	str += "Alamat: "+alamat+"<br>";
	str += "Agama: "+agama+"</p>";

	$('#isidata').html(str);
	
}