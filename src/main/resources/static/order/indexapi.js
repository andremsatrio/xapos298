

function newOrder() {

	var formdata = '{'
	formdata += '"reference":"",'
	formdata += '"amount":""'
	formdata += '}'

	$.ajax({
		url: "/api/orderheader/add",
		type: "post",
		contentType: "application/json",
		data: formdata,
		success: function() {
			$.ajax({
				url: "/api/maxorderheaderid",
				type: "get",
				contentType: "application/json",
				success: function(maxHeaderId) {
					$.ajax({
						url: "/api/product",
						type: "get",
						contentType: "application/json",
						success: function(data) {
							var str = "<table class='table'>"
							str += "<tr>"
							str += "<th>Product Name</th>"
							str += "<th>Price</th>"
							str += "<th>Quantity</th>"
							str += "<th>Amount</th>"
							str += "</tr><tr>"

							str += "<td><select class='form-control' id='productId' onchange='setPriceAndQuantity(this.value)'>"
							str += "<option selected>-- Select Product --</option>"
							for (var i = 0; i < data.length; i++) {
								str += "<option value=" + data[i].id + ">" + data[i].productName + "</option>"
							}
							str += "</select></td>"

							str += "<td><input class='form-control' type='text' id='productPrice' disabled></td>"

							str += "<td><input type='number' inputmode='numeric' min=1 class='form-control' id='quantity' onkeyup='validationInput()' onwheel='validationInput()'></td>"

							str += "<td><input type='text' id='amount' disabled class='form-control'></td>"

							str += "</tr><tr>"

							str += "<td><button class='btn btn-success' onclick='addOrder(" + maxHeaderId + ")'>Tambah Item</button></td>"

							str += "</tr>"
							str += "</table>"

							$("#isidata").html(str)
						}
					})
				}
			})

		}
	})
}
var tempStock = 0
function setPriceAndQuantity(id) {
	if (id > 0) {
		$.ajax({
			url: "/api/product/" + id,
			type: "get",
			contentType: "application/json",
			success: function(data) {
				$("#productPrice").val(data.productPrice)
				$("#quantity").attr("max", data.productStock)
				tempStock = data.productStock
			}
		})
	} else {
		$("#productPrice").val(0)
	}
}
function validationInput() {
	var quantity = $("#quantity").val()
	if (quantity > tempStock) {
		$("#quantity").val(tempStock)
	} else if (quantity < 1) {
		$("#quantity").val(1)
	}

	quantity = $("#quantity").val()
	var price = $("#productPrice").val()
	$("#amount").val(quantity * price)
}

function addOrder(headerId) {
	var productId = $("#productId").val()
	var price = $("#productPrice").val()
	var quantity = $("#quantity").val()
	var formdata = '{'
	formdata += '"headerId":' + headerId + ','
	formdata += '"productId":' + productId + ','
	formdata += '"price":' + price + ','
	formdata += '"quantity":' + quantity
	formdata += '}'

	$.ajax({
		url: "/api/orderdetail/add",
		type: "post",
		contentType: "application/json",
		data: formdata,
		success: function() {
			dataList(headerId)
		}
	})
}

var listData = []
function dataList(headerId) {
	$.ajax({
		url: "/api/orderdetail/header/" + headerId,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			var str = "<table class='table' cellpadding=5>"
			str += "<tr>"
			str += "<th>Reference</th>"
			str += "<th>Product</th>"
			str += "<th>Quantity</th>"
			str += "<th>Total</th>"
			str += "</tr>"

			listData = []
			var indeksListData = 0;

			var amountTotal = 0;
			for (var i = 0; i < data.length; i++) {
				var amount = data[i].price * data[i].quantity
				amountTotal += amount
				str += "<tr>"
				str += "<td>" + data[i].orderHeaders.reference + "</td>"
				str += "<td>" + data[i].product.productName + "</td>"
				str += "<td>" + data[i].quantity + "</td>"
				str += "<td>" + amount + "</td>"
				str += "</tr>"

				listData[indeksListData] = i + 1
				indeksListData += 1
				listData[indeksListData] = data[i].product.productName
				indeksListData += 1
				listData[indeksListData] = data[i].quantity
				indeksListData += 1
			}
			str += "<tr>"
			str += "<th colspan='3'>Total Amount</th>"
			str += "<td>" + amountTotal + "</td>"
			str += "</tr>"
			str += "<tr>"
			str += "<td colspan='3'></td>"
			str += "<td><button class='btn btn-primary' onclick='confirmCheckOut(" + headerId + "," + amountTotal + ")'>Checkout</button></td>"
			str += "</tr>"
			str += "</table>"

			$("#data-list").html(str)
		}
	})
}
function confirmCheckOut(headerId, amountTotal) {
	var str = "<h5>Apakah anda yakin ingin checkout barang ini : </h5>"
	str += "<table class='table table-border-borderless'>"
	str += "<thead>"
	str += "<tr>"
	str += "<th>No</th>"
	str += "<th>Product Name</th>"
	str += "<th>Quantity</th>"
	str += "</tr>"
	str += "</thead>"
	str += "<tbody>"

	for (var i = 0; i < listData.length; i++) {
		if (i % 3 == 0) {
			str += "<tr>"
			str += "<td>" + listData[i] + "</td>"
		} else if (i % 3 == 1) {
			str += "<td>" + listData[i] + "</td>"
		} else if (i % 3 == 2) {
			str += "<td>" + listData[i] + "</td>"
			str += "</tr>"
		}
		str += ""
	}
	listData = []

	str += ""
	str += "</tbody>"
	str += "</table>"

	$("#btn-save").off("click").on("click", function() {
		doneProcess(headerId, amountTotal)
	}).html("Checkout")
	$(".modal-title").html("Checkout")
	$(".modal-body").html(str)
	$("#modal").modal("show")
}
function doneProcess(headerId, amountTotal) {
	var formdata = "{"
	formdata += '"id":' + headerId + ','
	formdata += '"amount":' + amountTotal
	formdata += "}"

	$.ajax({
		url: "/api/orderheader/done",
		type: "put",
		contentType: "application/json",
		data: formdata,
		success: function() {
			alert("Checkout success")
			$("#modal").modal("toggle")

			window.location.href = "indexapi"
		}
	})
}

$(function() {
	getAllOrderHeader()

	$("#btn-search").on("click", searchOrderHeader)
	// Handling Enter key
	$(document).on("keydown", "#form-navbar", function(event) {
		if (event.keyCode == 13) {
			searchOrderHeader()
			event.preventDefault();
			return false;
		}
		return event.key != "Enter";
	});
})

function getAllOrderHeader() {
	$.ajax({
		url: "/api/orderheader",
		type: "get",
		contentType: "application/json",
		success: function(data) {
			var str = "<table class='table'>"
			str += "<thead>"
			str += "<tr>"
			str += "<th>No</th>"
			str += "<th>Reference</th>"
			str += "<th>Amount</th>"
			str += "</tr></thead>"
			str += "<tbody id='data-orderheader'>"
			for (var i = 0; i < data.length; i++) {
				str += "<tr>"
				str += "<td>" + (i + 1) + "</td>"
				str += "<td>" + data[i].reference + "</td>"
				str += "<td>" + data[i].amount + "</td>"
				str += "</tr>"
			}
			str += "</tbody>"
			str += "</table>"

			$("#isidata").html("")
			$("#isidata").html(str)
		}
	})
}

function searchOrderHeader() {
	console.log("searching..")
	var keyword = $("#input-search").val()

	if (keyword == null || keyword == "") {
		getAllOrderHeader()
	} else {
		$.ajax({
			url: "/api/orderheader/search/" + keyword,
			type: "get",
			contentType: "application/json",
			success: function(data) {
				console.log(data)
				var str = ""
				for (var i = 0; i < data.length; i++) {
					str += "<tr>"
					str += "<td>" + (i + 1) + "</td>"
					str += "<td>" + data[i].reference + "</td>"
					str += "<td>" + data[i].amount + "</td>"
					str += "</tr>"
				}

				$("#data-orderheader").html("");
				$("#data-orderheader").html(str);
			}
		})
	}
}