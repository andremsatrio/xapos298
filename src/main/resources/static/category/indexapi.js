$(function() {
	getAllCategory(0,2);

	$("#btn-search").on("click", searchCategory)
	// Handling Enter key
	$(document).on("keydown", "#form-navbar", function(event) {
		if (event.keyCode == 13) {
			searchCategory()
			event.preventDefault();
			return false;
		}
		return event.key != "Enter";
	});
})
function getAllCategory(currentPage, length) {
	$.ajax({
		url: '/api/category/paging?page='+currentPage+'&size='+length,
		type: 'get',
		contentType: 'application/json',
		success: function(rawData) {
			var data = rawData.category
			console.log( rawData.totalPage)
			console.log(currentPage)
			var str = "<table border='0' class='table'>";
			str += "<thread>";
			str += "<th>Inital</th>";
			str += "<th>Category</th>";
			str += "<th>Active</th>";
			str += "<th colspan='2'>Action</th>";
			str += "</thead>";
			str += "<tbody id='data-category'>";
			for (var i = 0; i < length; i++) {
				str += "<tr>";
				str += "<td>" + data[i].categoryInitial + "</td>";
				str += "<td>" + data[i].categoryName + "</td>";
				str += "<td><input type='checkbox' name='isactive' checked disabled></td>";
				str += "<td><button class='btn btn-warning' onClick='findDataForModal(" + data[i].id + ',' + false + ")'><i class='bi-pencil-square'></i></button>";
				str += "<button class='btn btn-danger' onClick='findDataForModal(" + data[i].id + ',' + true + ")'><i class='fas fa-trash'></i></button></td>";
				str += "<td><input type='checkbox' value=" + data[i].id + " onclick='selectedItem(this.value)' class='c_check'></td>"
				str += "</tr>";
			}
			str += "</tbody>";
			str += "</table>";

			str += "<br>"

			$("#isidata").html(str);



			str = ""
			if(currentPage > 0){
				str += '<li class="page-item" disabled><a id="page-previous" class="page-link" onclick="getAllCategory(' + (currentPage - 1) + ',' + length + ')" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>'
			} 
			
			for (var i = 0; i < rawData.totalPage; i++) {
				str += '<li class="page-item"><a class="page-link" onclick="getAllCategory(' + i + ',' + length + ')">' + (i + 1) + '</a></li>'
			}

			if(currentPage < rawData.totalPage-1){
				str += '<li class="page-item"><a class="page-link" onclick="getAllCategory(' + (currentPage + 1) + ',' + length + ')" aria-label="Next"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>'
			}
			
			$(".pagination").html("")
			$(".pagination").html(str)
			

		}
	})
}
function findDataForModal(id, isDelete) {
	$.ajax({
		url: '/api/category/' + id,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var initial = "";
			var name = "";
			var active = false;

			initial = data.categoryInitial;
			name = data.categoryName;
			active = data.isActive;

			if (!isDelete) {
				openModal(id, initial, name, active)
			} else {
				openModalDelete(id, initial, name, active)
			}
		}
	})
}

function openModalDelete(id, initial, name, active) {
	if (active) {
		active = "checked"
	} else {
		active = ""
	}
	var str = "<center>";
	str += "<h3>Are you sure you want to delete this?</h3><br>";
	str += "<input type='hidden' id='categoryId' value=" + id + ">"
	str += "<table>";
	str += "<tr>";
	str += "<td class='align-center'><b>Initial </b></td>";
	str += "<td>" + initial + "</td>";
	str += "</tr><tr>"
	str += "<td class='align-center'><b>Name </b></td>";
	str += "<td>" + name + "</td>";
	str += "</tr><tr>"
	str += "<td class='align-center'><b>Active </b></td>";
	str += "<td> <input type='checkbox' disabled id='isActive' " + active + "></td></tr>";
	str += "</table>";
	str += "</center>"

	$(".modal-title").html('Delete')
	$(".modal-body").html(str)
	$("#btn-delete").on('click', function() {
		deleteCategory()
		$(".modal-body").html("")
	})
	$("#modalDelete").modal('show')
}
function openModal(id, initial, name, active) {
	if (active) {
		active = "checked"
	}
	if (id == null) {
		initial = ""
		name = ""
		active = false
	}

	var str = ""
	str = "<form method='post'>"
	str += "<input type='hidden' value=" + id + " id='categoryId'>"
	str += "<div class='form-group'>"
	str += "<label>Category Initial</label>"
	str += "<input type='text' class='form-control' id='categoryInitial' value=" + initial + ">"
	str += "</div>"
	str += "<div class='form-group'>"
	str += "<label>Category Name</label>"
	str += "<input type='text' class='form-control' id='categoryName'  value=" + name + ">"
	str += "</div>"
	str += "<label>Active</label>"
	str += "<div class='form-group form-check'>"
	str += "<input type='checkbox' class='form-check-input' id='isActive' " + active + ">"
	str += "</div>"
	str += "</form>"

	$(".modal-title").html('Category Form')
	$(".modal-body").html(str)
	$("#btn-save").on('click', function() {
		saveCategory()
		$(".modal-body").html("")
	})
	$('#modal').modal('show')
}
function deleteCategory() {
	var id = $('#categoryId').val()
	$.ajax({
		url: '/api/delete/category/' + id,
		type: 'put',
		contentType: 'application/json',
		success: function(data) {
			$('#modalDelete').modal('toggle')
			getAllCategory();
		}
	})
}
function createCategory() {
	openModal(undefined, "", "", false);
}
function saveCategory() {
	var id = $('#categoryId').val()
	var initial = $('#categoryInitial').val()
	var name = $('#categoryName').val()
	var active = $('#isActive')[0].checked

	var formdata = '{'
	formdata += '"categoryInitial":"' + initial + '",'
	formdata += '"categoryName":"' + name + '",'
	formdata += '"isActive":' + active + ''
	formdata += '}'
	if (id == "undefined") {// if not edit, then save as new data
		$.ajax({
			url: '/api/category/add',
			type: 'post',
			contentType: 'application/json',
			data: formdata,
			success: function() {
				$('#modal').modal('toggle')
				getAllCategory()
			}
		})
	} else { // if edit
		$.ajax({
			url: '/api/edit/category/' + id,
			type: 'put',
			contentType: 'application/json',
			data: formdata,
			success: function() {
				$('#modal').modal('toggle')
				getAllCategory()
			}
		})
	}
}

function searchCategory() {
	console.log("searching..")
	var keyword = $("#input-search").val()

	if (keyword == null || keyword == "") {
		getAllCategory()
	} else {
		$.ajax({
			url: "/api/category/search/" + keyword,
			type: "get",
			contentType: "application/json",
			success: function(data) {
				//console.log(data)
				var str = ""
				for (var i = 0; i < data.length; i++) {
					str += "<tr>";
					str += "<td>" + data[i].categoryInitial + "</td>";
					str += "<td>" + data[i].categoryName + "</td>";
					str += "<td><input type='checkbox' name='isactive' checked disabled></td>";
					str += "<td><button class='btn btn-warning' onClick='findDataForModal(" + data[i].id + ',' + false + ")'><i class='bi-pencil-square'></i></button>";
					str += "<button class='btn btn-danger' onClick='findDataForModal(" + data[i].id + ',' + true + ")'><i class='fas fa-trash'></i></button></td>";
					str += "</tr>";
				}

				$("#data-category").html("");
				$("#data-category").html(str);
			}
		})
	}
}

function selectedItem(id) {
	var check = $(".c_check")
	var selected = []
	for (var i = 0; i < check.length; i++) {
		if (check[i].checked) {
			selected.push(check[i].value)
		}
	}

	var btnMultiple = $("#btn-multiple")
	if (selected.length > 0) {
		btnMultiple.attr("disabled", false).off("click").on("click", function() {
			confirmMultiple(selected)
		})
	} else {
		btnMultiple.attr("disabled", true)
	}
}

function confirmMultiple(select) {
	var str = "Apakah anda yakin menghapus category ini: <br>"
	for (var i = 0; i < select.length; i++) {
		$.ajax({
			url: "/api/category/" + select[i],
			type: "get",
			contentType: "application/json",
			success: function(data) {
				str += "<span>" + data.categoryName + "</span><br>"
				$(".modal-title").html("<h5>Hapus Category</h5>")
				$(".modal-body").html(str)
				$("#btn-save").html("Delete").off("click").on("click", function() {
					deleteMultiple(select)
				})
				$("#modal").modal("show")
			}
		})
	}
}

function deleteMultiple(items) {
	for (var i = 0; i < items.length; i++) {
		$.ajax({
			url: "/api/delete/category/" + items[i],
			type: "put",
			contentType: "application/json",
			success: function() {
				$("#modal").modal("toggle")
				getAllCategory()
			}
		})
	}
}