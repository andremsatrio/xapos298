$(function() {
	getAllProduct();

	$("#btn-search").on("click", searchProduct)
	// Handling Enter key
	$(document).on("keydown", "#form-navbar", function(event) {
		if (event.keyCode == 13) {
			searchProduct()
			event.preventDefault();
			return false;
		}
		return event.key != "Enter";
	});
})
function getAllProduct() {
	$.ajax({
		url: '/api/product',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			// id, category, variant, initial, name, description, price, stock, active
			var str = "<table class='table'>"
			str += "<thead>"
			str += "<th>Category / Variant</th>"
			str += "<th>Initial</th>"
			str += "<th>Name</th>"
			str += "<th>Description</th>"
			str += "<th>Price</th>"
			str += "<th>Stock</th>"
			str += "<th>Active</th>"
			str += "<th>Action</th>"
			str += "</thead>"
			str += "<tbody id='data-product'>"

			for (var i = 0; i < data.length; i++) {
				str += "<tr>"

				str += "<td>" + data[i].variant.category.categoryName + "/" + data[i].variant.variantName + "</td>"
				str += "<td>" + data[i].productInitial + "</td>"
				str += "<td>" + data[i].productName + "</td>"
				str += "<td>" + data[i].productDescription + "</td>"
				str += "<td>" + data[i].productPrice + "</td>"
				str += "<td>" + data[i].productStock + "</td>"
				str += "<td>" + "<input type='checkbox' name='isActive' checked disabled>" + "</td>"

				str += "<td><button class='btn btn-warning' onClick='findDataForModal(" + data[i].id + "," + "false" + ")'><i class='bi-pencil-square'></i></button>"
				str += "<button class='btn btn-danger' onClick='findDataForModal(" + data[i].id + "," + "true" + ")'><i class='bi-trash'></i></button></td>"
				str += "</tr>"
				str += ""
			}

			str += "</tbody></table>"

			$("#isidata").html(str)
		}
	})
}
function findDataForModal(id, isDelete) {
	$.ajax({
		url: '/api/product/' + id,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var category = data.variant.category.categoryName
			var variant = data.variant.variantName
			var variantId = data.variant.id
			var initial = data.productInitial
			var name = data.productName
			var description = data.productDescription
			var price = data.productPrice
			var stock = data.productStock
			var active = data.isActive
			if (!isDelete) {
				openModal(id, category, variant, variantId, initial, name, description, price, stock, active)
			} else {
				openModalForDelete(id, category, variant, initial, name, description, price, stock, active)
			}
		}
	})

}
function createProduct() {
	openModal(undefined, "", "", "", "", "", "", "", "", false);
}
function openModal(id, category, variant, variantId, initial, name, description, price, stock, active) {
	if (active) {
		active = "checked"
	}
	if (id == null) {
		initial = ""
		name = ""
		active = false
		category = " -- Pilih Category -- "
		variant = " -- Pilih Variant -- "
		price = ""
		stock = ""
		description = ""
	}

	$.ajax({
		url: '/api/category',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {

			var str = "<form>"
			str += "<input type='hidden' id='productId' value=" + id + ">"
			str += "<div class='form-group'>"

			str += "<label>Category</label>"
			str += "<select id='categoryId' class='custom-select' onchange='variantByCategory(this.value," + variantId + ")'>"
			if (id == null) {
				str += "<option selected value='-1'> -- Pilih Category -- </option>"
			}
			for (var i = 0; i < data.length; i++) {
				var selected = ""
				if (data[i].categoryName == category) {
					selected = "selected"

					variantByCategory(data[i].id, variantId)
				}

				str += "<option text='categoryName' " + selected + " value='" + data[i].id + "'>" + data[i].categoryName + "</option>"
			}
			str += "</select><br>"
			str += "<label>Variant</label>"
			str += "<select id='variantId' class='custom-select'>"
			if (id == null) {
				str += "<option selected value='-1'>" + variant + "</option>"
			} else {
				str += "<option selected value='" + variantId + "'>" + variant + "</option>"
			}
			str += "</select><br>"

			str += "<label>Initial</label><br>"
			str += "<input type='text' class='form-control' id='productInitial' value='" + initial + "'>"
			str += "<label>Name</label>"
			str += "<input type='text' class='form-control' id='productName' value='" + name + "'>"

			str += "<label>Description</label>"
			str += "<input type='text' class='form-control' id='productDescription' value='" + description + "'>"
			str += "<label>Price</label>"
			str += "<input type='number' class='form-control' id='productPrice' min=0 value='" + price + "'>"
			str += "<label>Stock</label>"
			str += "<input type='number' class='form-control' id='productStock' min=0 value='" + stock + "'>"

			str += "<label>Active</label>"
			str += "<div class='form-group form-check'>"
			str += "<input type='checkbox' class='form-check-input' id='isActive' " + active + ">"
			str += "</div>"

			str += ""
			str += ""
			str += "</div></form>"

			if (id != null) { // edit data
				$("#btn-save").css("background-color", "orange")
				$("#btn-save").css("border-color", "orange")
				$("#btn-save").text("Save changes")
				$(".modal-title").html("Edit")
			} else { // add new data
				$("#btn-save").css("background-color", "green")
				$("#btn-save").css("border-color", "green")
				$("#btn-save").text("Create")
				$(".modal-title").html("Create")
			}

			$(".modal-body").html(str)
			$("#btn-save").off("click").on("click", function() {
				saveProduct()
			})
			$("#modal").modal("show")


		}
	})


}

function openModalForDelete(id, category, variant, initial, name, description, price, stock, active) {
	if (active) {
		active = "checked"
	} else {
		active = ""
	}
	var str = ""
	str += "<center>"
	str += "<h3> Are you sure you want to delete this?</h3>"
	str += "<table>"
	str += "<tr>"
	str += "<td class='align-right'><b>Category/Variant</b></td>"
	str += "<td>" + category + "/" + variant + "</td>"
	str += "</tr>"
	str += "<tr>"
	str += "<td class='align-right'><b>Initial</b></td>"
	str += "<td>" + initial + "</td>"
	str += "</tr><tr>"
	str += "<td class='align-right'><b>Name</b></td>"
	str += "<td>" + name + "</td>"
	str += "</tr><tr>"
	str += "<td class='align-right'><b>Description</b></td>"
	str += "<td>" + description + "</td>"
	str += "</tr><tr>"
	str += "<td class='align-right'><b>Price</b></td>"
	str += "<td>" + price + "</td>"
	str += "</tr><tr>"
	str += "<td class='align-right'><b>Stock</b></td>"
	str += "<td>" + stock + "</td>"
	str += "</tr><tr>"
	str += "<td class='align-right'><b>Active</b></td>"
	str += "<td><input type='checkbox' disabled id='isActive' " + active + "></td>"
	str += "</tr>"
	str += "</table></center>"

	$("#btn-save").css("background-color", "red")
	$("#btn-save").css("border-color", "red")
	$("#btn-save").text("Delete")
	$(".modal-title").html("Delete")
	$(".modal-body").html(str)
	$("#btn-delete").off("click").on("click", function() {
		deleteProduct(id)
	})
	$("#modalDelete").modal("show")
}
function deleteProduct(id) {
	$.ajax({
		url: '/api/delete/product/' + id,
		type: 'put',
		contentType: 'application/json',
		success: function(data) {
			$('#modalDelete').modal('toggle')
			getAllProduct();
		}
	})
}

function saveProduct() {
	var variantId = $("#variantId").val()

	if (variantId != "-1") {


		var id = $("#productId").val()
		var initial = $("#productInitial").val()
		var name = $("#productName").val()
		var description = $("#productDescription").val()
		var price = $("#productPrice").val()
		var stock = $("#productStock").val()
		var active = $("#isActive")[0].checked

		var formdata = "{"
		formdata += '"variantId":' + variantId + ','
		formdata += '"productInitial":"' + initial + '",'
		formdata += '"productName":"' + name + '",'
		formdata += '"productDescription":"' + description + '",'
		formdata += '"productPrice":' + price + ','
		formdata += '"productStock":' + stock + ','
		formdata += '"isActive":' + active
		formdata += '}'

		if (id == "undefined") {
			$.ajax({
				url: '/api/product/add',
				type: 'post',
				contentType: 'application/json',
				data: formdata,
				success: function() {
					$("#modal").modal("toggle")
					getAllProduct()
				}
			})
		} else {
			$.ajax({
				url: '/api/edit/product/' + id,
				type: 'put',
				contentType: 'application/json',
				data: formdata,
				success: function() {
					$("#modal").modal("toggle")
					getAllProduct()
				}
			})
		}

	} else {
		$("#modal").modal("toggle")
	}


}


function variantByCategory(id, variantId) {
	$.ajax({
		url: '/api/variant/category/' + id,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var selected = ""
			var str = ""
			for (var i = 0; i < data.length; i++) {
				if (data[i].id == variantId) {
					selected = "selected"
				} else {
					selected = ""
				}
				str += "<option text='variantName' " + selected + " value='" + data[i].id + "'>" + data[i].variantName + "</option>"
				//console.log(data[i].variantName);
			}
			$("#variantId").html(str)
		}
	})
}

function searchProduct() {
	console.log("searching..")
	var keyword = $("#input-search").val()

	if (keyword == null || keyword == "") {
		getAllProduct()
	} else {
		$.ajax({
			url: "/api/product/search/" + keyword,
			type: "get",
			contentType: "application/json",
			success: function(data) {
				var str = ""
				for (var i = 0; i < data.length; i++) {
					str += "<tr>"

					str += "<td>" + data[i].variant.category.categoryName + "/" + data[i].variant.variantName + "</td>"
					str += "<td>" + data[i].productInitial + "</td>"
					str += "<td>" + data[i].productName + "</td>"
					str += "<td>" + data[i].productDescription + "</td>"
					str += "<td>" + data[i].productPrice + "</td>"
					str += "<td>" + data[i].productStock + "</td>"
					str += "<td>" + "<input type='checkbox' name='isActive' checked disabled>" + "</td>"

					str += "<td><button class='btn btn-warning' onClick='findDataForModal(" + data[i].id + "," + "false" + ")'><i class='bi-pencil-square'></i></button>"
					str += "<button class='btn btn-danger' onClick='findDataForModal(" + data[i].id + "," + "true" + ")'><i class='bi-trash'></i></button></td>"
					str += "</tr>"
				}

				$("#data-product").html("");
				$("#data-product").html(str);
			}
		})
	}
}
