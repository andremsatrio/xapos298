$(function() {
	getAllVariant();

	$("#btn-search").on("click", searchVariant)
	// Handling Enter key
	$(document).on("keydown", "#form-navbar", function(event) {
		if (event.keyCode == 13) {
			searchVariant()
			event.preventDefault();
			return false;
		}
		return event.key != "Enter";
	});
})

function getAllVariant() {
	$.ajax({
		url: '/api/variant',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = "<table border='0' class='table'>";
			str += "<thread>";
			str += "<th>Category</th>";
			str += "<th>Initial</th>";
			str += "<th>Name</th>";
			str += "<th>Active</th>"
			str += "<th colspan='2'>Action</th>";
			str += "</thead>";
			str += "<tbody id='data-variant'>";
			for (var i = 0; i < data.length; i++) {
				str += "<tr>";
				str += "<td>" + data[i].category.categoryName + "</td>";
				str += "<td>" + data[i].variantInitial + "</td>";
				str += "<td>" + data[i].variantName + "</td>";
				str += "<td><input type='checkbox' name='isactive' checked disabled></td>";
				str += "<td><button class='btn btn-warning' onClick='findDataForModal(" + data[i].id + "," + false + ")'><i class='bi-pencil-square'></i></button>";
				str += "<button class='btn btn-danger' onCLick='findDataForModal(" + data[i].id + "," + true + ")'><i class='bi-trash'></i></button></td>";
				str += "</tr>";
			}

			str += "</tbody></table>";
			$("#isidata").html(str);
		}
	})
}

function findDataForModal(id, isDelete) {
	$.ajax({
		url: '/api/variant/' + id,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var category = data.category.categoryName;
			var initial = data.variantInitial;
			var name = data.variantName;
			var active = data.isActive;

			if (!isDelete) {
				openModal(id, category, initial, name, active)
			} else {
				openModalDelete(id, category, initial, name, active)
			}

		}
	})
}
function openModal(id, category, initial, name, active) {
	if (active) {
		active = "checked"
	}
	if (id == null) {
		initial = ""
		name = ""
		active = false
		category = " -- Pilih Category -- "
	}

	$.ajax({
		url: '/api/category',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = "<form>"
			str += "<input type='hidden' value=" + id + " id='variantId'>"
			str += "<div class='form-group'>"
			str += "<lable>Category</label><br>"
			str += "<select id='categoryId' class='custom-select'>"
			if (id == null) {
				str += "<option selected value='-1'>-- Pilih Category -- </option>"
			}
			for (var i = 0; i < data.length; i++) {
				var selected = ""
				if (data[i].categoryName == category) {
					selected = "selected"
				}

				str += "<option text='CategoryName' value=" + data[i].id + " " + selected + ">" + data[i].categoryName + "</option>"
			}
			str += "</select><br>"
			str += "<label>Initial</label>"
			str += "<input type='text' class='form-control' id='variantInitial' value='" + initial + "'>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Name</label>"
			str += "<input type='text' class='form-control' id='variantName' value='" + name + "'>"
			str += "</div>"
			str += "<label>Active</label>"
			str += "<div class='form-group form-check'>"
			str += "<input type='checkbox' class='form-check-input' id='isActive' " + active + ">"
			str += "</div>"

			str += "</form>"


			$(".modal-title").html('Variant Form')
			$(".modal-body").html(str)
			$("#btn-save").off('click').on('click', function() {
				saveVariant()
			})
			$('#modal').modal('show')
		}
	})
}
function openModalDelete(id, category, initial, name, active) {
	if (active) {
		active = "checked"
	} else {
		active = ""
	}

	var str = ""
	str += "<center>"
	str += "<h3>Are you sure you want to delete this?</h3>"
	str += "<table>"
	str += "<tr>"
	str += "<td class='align-right'><b>Category</b></td>"
	str += "<td>" + category + "</td>"
	str += "</tr>"
	str += "<tr>"
	str += "<td class='align-right'><b>Initial</b></td>"
	str += "<td>" + initial + "</td>"
	str += "</tr><tr>"
	str += "<td class='align-right'><b>Name</b></td>"
	str += "<td>" + name + "</td>"
	str += "</tr><tr>"
	str += "<td class='align-right'><b>Active</b></td>"
	str += "<td><input type='checkbox' disabled id='isActive' " + active + "></td></tr>"
	str += "</table>"
	str += "</center>"

	$(".modal-title").html("Delete")
	$(".modal-body").html(str)
	$("#btn-delete").off('click').on('click', function() {
		deleteVariant(id)
	})
	$("#modalDelete").modal('show')
}
function deleteVariant(id) {
	$.ajax({
		url: '/api/delete/variant/' + id,
		type: 'put',
		contentType: 'application/json',
		success: function(data) {
			$('#modalDelete').modal('toggle')
			getAllVariant();
		}
	})
}
function createVariant() {
	openModal(undefined, "", "", "", false)
}
function saveVariant() {
	var categoryId = $('#categoryId').val()

	console.log(categoryId)

	if (!(categoryId === "-1")) {
		var id = $('#variantId').val()

		var initial = $('#variantInitial').val()
		var name = $('#variantName').val()
		var active = $('#isActive')[0].checked

		var formdata = '{'
		formdata += '"categoryId":' + categoryId + ','
		formdata += '"variantInitial":"' + initial + '",'
		formdata += '"variantName":"' + name + '",'
		formdata += '"isActive":' + active + ''
		formdata += "}"

		if (id == "undefined") {
			$.ajax({
				url: '/api/variant/add',
				type: 'post',
				contentType: 'application/json',
				data: formdata,
				success: function() {
					$('#modal').modal('toggle')
					getAllVariant();
				}
			})
		} else {
			$.ajax({
				url: '/api/edit/variant/' + id,
				type: 'put',
				contentType: 'application/json',
				data: formdata,
				success: function() {
					$('#modal').modal('toggle')
					getAllVariant()
				}
			})
		}
	} else {
		$('#modal').modal('toggle')
	}
}

function searchVariant() {
	console.log("searching..")
	var keyword = $("#input-search").val()

	if (keyword == null || keyword == "") {
		getAllVariant()
	} else {
		$.ajax({
			url: "/api/variant/search/" + keyword,
			type: "get",
			contentType: "application/json",
			success: function(data) {
				var str = ""
				for (var i = 0; i < data.length; i++) {
					str += "<tr>";
					str += "<td>" + data[i].category.categoryName + "</td>";
					str += "<td>" + data[i].variantInitial + "</td>";
					str += "<td>" + data[i].variantName + "</td>";
					str += "<td><input type='checkbox' name='isactive' checked disabled></td>";
					str += "<td><button class='btn btn-warning' onClick='findDataForModal(" + data[i].id + "," + false + ")'><i class='bi-pencil-square'></i></button>";
					str += "<button class='btn btn-danger' onCLick='findDataForModal(" + data[i].id + "," + true + ")'><i class='bi-trash'></i></button></td>";
					str += "</tr>";
				}

				$("#data-variant").html("");
				$("#data-variant").html(str);
			}
		})
	}
}